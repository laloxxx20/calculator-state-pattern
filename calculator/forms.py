from django import forms
from calculator.calculatorControl import *

class Calculate(forms.Form):
	first = forms.DecimalField(widget = forms.TextInput(attrs = {'class' : 'form_input', 'id':'first'}))
	value = forms.CharField(widget = forms.TextInput(attrs = {'class':"uneditable-input", 'id':'value'}))
	second = forms.DecimalField(widget = forms.TextInput(attrs = {'class' : 'form_input', 'id':'second'}))	

	def clean(self):		
		cleaned_data = super(Calculate, self).clean()				
		return cleaned_data	

	def values(self):
		first=self.cleaned_data['first']
		value=self.cleaned_data['value']
		second=self.cleaned_data['second']
		print "first: ",first
		print "value: ",value
		print "second: ",second 
		
		rpta=None
		if value=="+":
			rpta=Sum(float(first),float(second))
		elif value=="-":	
			rpta=Min(float(first),float(second))	
		elif value=="*":
			rpta=Mul(float(first),float(second))
		elif value=="/":
			rpta=Div(float(first),float(second))

		objectStrategy=StrategyCalculator(rpta) # here we applicate the Strategy pattern 
		totalRpta=objectStrategy.doit()	

		return totalRpta


