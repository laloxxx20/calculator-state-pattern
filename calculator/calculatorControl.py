
class StrategyCalculator:
    def __init__(self, object):
        self.whatMake= object
    	
    def doit(self):
        return self.whatMake.method(self.whatMake.first,self.whatMake.second)


class Sum:	
	def __init__(self,first,second):
		self.first=first
		self.second=second

	def method(self, first, second):		
		return first+second
        
class Min:
	def __init__(self, first,second):
		self.first=first
		self.second=second		

	def method(self, first, second):
		return first-second
        
class Div:
	def __init__(self, first,second):
		self.first=first
		self.second=second		

	def method(self, first, second):       	
		return first/second

class Mul:
	def __init__(self, first,second):
		self.first=first
		self.second=second		

	def method(self, first, second):        
		return first*second