from django.http import HttpResponse
from django.shortcuts import get_object_or_404, render, get_list_or_404, render_to_response
from calculator.calculatorControl import *
from calculator.forms import *

def index(request):		
	context = {'rpta':0}
	
	if request.method == 'POST':
		formCalcu = Calculate(request.POST)
		if formCalcu.is_valid():
			print "form is valid"
			rptaSinceForm=formCalcu.values()
			print "rptaSinceForm: ",rptaSinceForm
			context['rpta']=rptaSinceForm
	else:
		formCalcu = Calculate()
		print "nada"

	context["form"]=formCalcu
	return render(request,'index.html',context)	

